import  serial
from datetime import datetime

debug=False

ser = serial.Serial('/dev/ttyUSB0', 2400, parity=serial.PARITY_NONE, timeout=2)
scale = 16
reading=0

while 1:
    with open("vp.log", "a") as logfile:
        
        now=datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print(now)
        logfile.write(now+",")

        h_byte=[]
        b_byte=[]
        
        word=ser.read(22).hex()
        
        """
        todo, make sure it starts reading at the start of the word and not in the mid.
        code below does not solve this.

        if word[0:2]!="02":

            print("not starts with 02")
            ser.reset_input_buffer()
            continue
        else:
            print("starts with 02")
        """    
        for x in range(len(word)):
            h_byte.append(word[x*2:x*2+2])
            #b_byte.append(bin(int(h_byte[x], scale))[2:].zfill(8))

        print(f"\n {word} --------------------------- reading: {reading}")
        reading=reading+1

        print("useful info:\n")
        logfile.write(str(int(h_byte[1],16))+",")
        print(f"towards radiators. temp: {int(h_byte[1],16)} °C")

        logfile.write(str(int(h_byte[2],16))+",")
        print(f"outdoor temp EcoLogic: {int(h_byte[2],16)-40} °C")

        logfile.write(str(int(h_byte[3],16)+int(h_byte[4],16)/10)+",")
        print(f"indoor temp: {int(h_byte[3],16)+int(h_byte[4],16)/10} °C")

        logfile.write(str(int(h_byte[5],16))+",")
        print(f"radiator return (before VP) temp: {int(h_byte[5],16)} °C")

        logfile.write(str(    int(h_byte[10], 16))+",")
        print(f"heat gas temp: {int(h_byte[10], 16)} °C")
        
        #todo amp does not seem to be correct, shows 0 or sometimes 1
        logfile.write(str(int(h_byte[13]))+",")    
        print(f"ampere: {int(h_byte[13])} A")
        
        logfile.write(str(int(h_byte[15],16)-40)+",")
        print(f"outdoor temp VP: {int(h_byte[15],16)-40} °C")
        
        logfile.write(str(int(h_byte[16],16))+",")
        print(f"after VP temp: {int(h_byte[16],16)} °C")

        if(debug==True):
        
            print(f"byte1 - okand, alltid 0x02? hex: {h_byte[0]}")

            print(f"byte7 - panna nedre temp: {int(h_byte[6],16)} C")

            print(f"byte8 - panna ovre temp: {int(h_byte[7],16)} C")

            print(f"byte9 - okand alltid 0x94?: hex: {h_byte[8]}")

            print(f"byte10 - okand alltid 0x94?: hex: {h_byte[9]}")

            print(f"byte12 - okand alltid 0x80?: hex: {h_byte[11]}")

            print(f"byte13 - okand alltid 0x00?: hex: {h_byte[12]}")

            print(f"byte15 - okand alltid 0x08?: hex: {h_byte[14]}")

            print(f"byte18 - okand alltid 0x00?: hex: {h_byte[17]}")

            print(f"byte19 - okand alltid 0x00?: hex: {h_byte[18]}")

            print(f"byte20 - sek raknare: {int(h_byte[19],16)} sek")

            print(f"byte21 - okand alltid 0x00?: hex: {h_byte[20]}")

            print(f"byte22 - okand alltid 0x00?: hex: {h_byte[21]}")

        logfile.write(" ,")

        logfile.write(str(h_byte[0])+",")
        logfile.write(str(h_byte[6])+",")
        logfile.write(str(h_byte[7])+",")
        logfile.write(str(h_byte[8])+",")
        logfile.write(str(h_byte[9])+",")
        logfile.write(str(h_byte[11])+",")
        logfile.write(str(h_byte[12])+",")
        logfile.write(str(h_byte[14])+",")
        logfile.write(str(h_byte[17])+",")
        logfile.write(str(h_byte[18])+",")
        logfile.write(str(h_byte[19])+",")
        logfile.write(str(h_byte[20])+",")
        logfile.write(str(h_byte[21])+",")

        logfile.write("\n")
ser.close()  

