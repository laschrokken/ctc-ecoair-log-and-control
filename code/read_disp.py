import  serial
from datetime import datetime

debug=False
ser = serial.Serial('/dev/ttyAMA0', 2400, parity=serial.PARITY_NONE, timeout=2)
scale = 16
reading=0
while 1:
    with open("display.log", "a") as logfile:
        
        now=datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print(now)
        logfile.write(now+",")
        h_byte=[]
        b_byte=[]
        
        word=ser.read(10).hex()
        for x in range(len(word)):
            h_byte.append(word[x*2:x*2+2])
            b_byte.append(bin(int(h_byte[x], scale))[2:].zfill(8))

        print(f"\n {word} --------------------------- reading: {reading}")
        reading=reading+1

        print("useful info:")

        logfile.write(str(b_byte[1][0])+",")
        print(f"compressor: {b_byte[1][0]}")

        logfile.write(str(b_byte[1][1])+",")        
        print(f"fan low: {b_byte[1][1]}")
        
        logfile.write(str(b_byte[1][3])+",")
        print(f"fan high: {b_byte[1][3]}")
        
        logfile.write(str(b_byte[2][0])+",")
        print(f"alarm led: {b_byte[2][0]}")
        
        logfile.write(str(b_byte[5][7])+",")
        print(f"radiatorpump: {b_byte[5][7]}")

        if(debug==True):
            print(f"\nbyte1 - okand, alltid 0xFA? hex: {h_byte[0]}")


            print(f"\nbyte2 - bitmapp {h_byte[1]}")
            print(b_byte[1])

    #        print(f"compressor: {b_byte[1][0]}")
    #        print(f"fan low: {b_byte[1][1]}")
            print(f"unkown: {b_byte[1][2]}")
    #        print(f"fan high: {b_byte[1][3]}")
            print(f"unknown: {b_byte[1][4]}")
            print(f"vaxelvantil VV=1 rad/VV=0: {b_byte[1][5]}")
            print(f"unknown: {b_byte[1][6]}")
            print(f"shunt2 close: {b_byte[1][7]}")

            print(f"\nbyte3 - bitmapp {h_byte[2]}")
            print(f"byte3 - bitmapp {b_byte[2]}")

            print(f"laddpump: {b_byte[2][1]}")
            print(f"unknown: {b_byte[2][2]}")
            print(f"unknown: {b_byte[2][3]}")
            print(f"unknown: {b_byte[2][4]}")
            print(f"spatsvarme inv.: {b_byte[2][5]}")
            print(f"shunt closing: {b_byte[2][6]}")
            print(f"shunt opening: {b_byte[2][7]}")

            print(f"\nbyte4 - samma som byte2: {h_byte[3]}\n")

            print(f"\nbyte5 - samma som byte3: {h_byte[4]}\n")
            print(f"\nbyte6 - bitmapp: {h_byte[5]}\n")

            print(f"unknown: {b_byte[5][0]}")
            print(f"unknown: {b_byte[5][1]}")
            print(f"unkown: {b_byte[5][2]}")
            print(f"unknown: {b_byte[5][3]}")
            print(f"unknown: {b_byte[5][4]}")
            print(f"shunt2 open: {b_byte[5][5]}")
            print(f"unknown: {b_byte[5][6]}")

            print(f"\nbyte7 - okand alltid 0x00?: {h_byte[6]}\n")

            print(f"\nbyte8 - okand alltid 0x00?: {h_byte[7]}")

            print(f"\nbyte9 - okand alltid 0x00 eller 0x80?: hex: {h_byte[8]}")

            print(f"\nbyte10 - okand alltid 0xFA eller 0x7A?: hex: {h_byte[9]}")
    """
    De tv       sista bytes       r 0x00 0xFA i det f      rsta blocket efter sp      nningstillslag, d      refter 0x80 0x7A.
    """
        logfile.write(" ,")

        logfile.write(str(h_byte[0])+",")
        logfile.write(str(h_byte[1])+",")
        logfile.write(str(h_byte[2])+",")
        logfile.write(str(h_byte[3])+",")
        logfile.write(str(h_byte[4])+",")
        logfile.write(str(h_byte[5])+",")
        logfile.write(str(h_byte[6])+",")
        logfile.write(str(h_byte[7])+",")
        logfile.write(str(h_byte[8])+",")
        logfile.write(str(h_byte[9])+",")
        
        logfile.write("\n")
ser.close()  

